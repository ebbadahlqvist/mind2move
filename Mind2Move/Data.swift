//
//  Data.swift
//  Mind2Move
//
//  Created by Ebba on 2016-04-17.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class Data: NSObject {
    
    var move = Movement(name: "Sträck",info: "Upp med armarna och sträck, passa också på att lyfta blicken från skärmen", intervall: 700, imageName:"gstretch")
    var move2 = Movement(name: "Lyft axlarna",info: "Sitt avslappnad med rak rygg. Dra upp båda axlarna mot öronen och släpp sedan ner dem igen och slappna av. Upprepa ett par gånger.", intervall: 700, imageName:"gshoulders")
    var move3 = Movement(name: "stretch av nacken",info: "Lägg ena armen på huvudet och stretcha försiktigt nacken, upprepa andra sidan", intervall: 700, imageName:"gneck")
    var move4 = Movement(name: "the bird",info: "Dra tillbaka huvudet och hakan så långt du kan. Upprepa några gånger", intervall: 700, imageName:"gbird")
    var move5 = Movement(name: "Kafferast",info: "Lämna datorn en stund, kanske en kopp kaffe?", intervall: 700, imageName:"coffe")
    
    var moveList1 = [Movement]()
    
    override init() {
        self.moveList1 = [Movement]()
        moveList1.append(move)
        moveList1.append(move2)
        moveList1.append(move3)
        moveList1.append(move4)
        moveList1.append(move5)
       
    }
   
    
}
