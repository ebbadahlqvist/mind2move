//
//  savedWorkdayDetailView.swift
//  Mind2Move
//
//  Created by Ebba on 2016-04-18.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import MessageUI
import Social



class savedWorkdayDetailView: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var clockLabel: UILabel!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var movementsDoneLabel: UILabel!
    @IBOutlet weak var notesLabel: UITextView!
    
    var dateText = ""
    var timeText = ""
    var notesText = ""
    var movementsDoneText = ""
    var clockText = ""
    
    override func viewDidLoad() {
        
        let customFont = UIFont(name: "Aloha", size: 17.0)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)
        navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        navigationItem.title = dateText
       
        timeLabel.text = timeText
        
        if(notesText == "Anteckningar..."){
         notesLabel.text = "Inga anteckningar"
        } else {
            notesLabel.text = notesText
        }
        
        movementsDoneLabel.text = movementsDoneText
        clockLabel.text = clockText
        
        let myImageName = dateText+clockLabel.text!
        let imagePath = fileInDocumentsDirectory(myImageName)
        
        if let loadedImage = loadImageFromPath(imagePath) {
            print(" Loaded Image: \(loadedImage)")
            
            imageView.image = loadedImage
            imageView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI / 2))
            
        } else { print("some error message 2") }
        
        
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true

    }
    @IBAction func emailButton(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
       
        mailComposerVC.setSubject("Arbetstid " + dateText)
        mailComposerVC.setMessageBody(createEmailText(), isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alertView = UIAlertController(title: "Kunde inte skicka", message: "Din telefon kunde inte skicka mail.", preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertView, animated: true, completion: nil)
        alertView.view.tintColor = UIColor.blackColor()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func facebookButton(sender: AnyObject) {
        let screen = UIScreen.mainScreen()
        
        if let window = UIApplication.sharedApplication().keyWindow {
            UIGraphicsBeginImageContextWithOptions(screen.bounds.size, false, 0);
            window.drawViewHierarchyInRect(window.bounds, afterScreenUpdates: false)
            let image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            let composeSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            composeSheet.setInitialText("")
            composeSheet.addImage(image)
            
            presentViewController(composeSheet, animated: true, completion: nil)
        }
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }
    
    func createEmailText() ->String {
        
        let emailText = dateText + "\n\n" + clockText + "\n" + timeText + "\n" + movementsDoneText + "\n\n" + notesLabel.text + "\n\n Detta mailet är skapat utifrån mina arbetstider i Mind2Move."
        
        return emailText
        
    }
  
}



