//
//  Movement.swift
//  Mind2Move
//
//  Created by Ebba on 2016-03-29.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class Movement {

        var name: String = ""
        var info: String = ""
        var intervall: Int = 0
        var imageName: String = ""
    
    init(name:String, info:String, intervall:Int, imageName:String){
        
            self.name = name;
            self.info = info;
            self.intervall = intervall;
            self.imageName = imageName;
            
        }
}
