//
//  EditMoveController.swift
//  Mind2Move
//
//  Created by Ebba on 2016-04-13.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit





class EditMoveController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var clockLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notesLabel: UITextView!
    @IBOutlet weak var doneMovementsLabel: UILabel!

    var imagePicker: UIImagePickerController!
    
    var totalTime = 0
    var doneMovementsNumber = 0
    
    var startTime = ""
    var dateText = ""
    
    let date = NSDate()
    let unitFlags: NSCalendarUnit = [.Day, .Month, .Year]
    
    var movements: [Dictionary<String, String>] = []
    var workDay: Dictionary<String, String> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customFont = UIFont(name: "Aloha", size: 17.0)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)
        
        notesLabel.text = "Anteckningar..."
        clockLabel.text = "Från klockan " + startTime + " till " + timeString()
        navigationItem.setHidesBackButton(true, animated:true)
        doneMovementsLabel.text = "Utförda rörelser: " + String(doneMovementsNumber)
        timeLabel.text = "Tid: "+timeStringAsText(totalTime)
        let components = NSCalendar.currentCalendar().components(unitFlags, fromDate: date)
        
        dateText = String(components.day) + " " + getMonth(components.month) + " "+String(components.year)
        navigationItem.title = dateText
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EditMoveController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let workData = NSUserDefaults.standardUserDefaults().objectForKey("workdays") as? NSData
        
        if let workData = workData {
            movements = NSKeyedUnarchiver.unarchiveObjectWithData(workData) as! [Dictionary<String,String>]
            print(movements)
        } else {
            movements = []
        }
        
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(EditMoveController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(EditMoveController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeStringAsText (seconds : Int) -> String {
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds)
        return "\(h) h \(m) min \(s) sek"
    }
    
    func getMonth (monthNumber : Int) -> String{
        
        switch monthNumber {
            case 1:
                return "Januari"
            case 2:
                return "Februari"
            case 3:
                return "Mars"
            case 4:
                return "April"
            case 5:
                return "Maj"
            case 6:
                return "Juni"
            case 7:
                return "Juli"
            case 8:
                return "Augusti"
            case 9:
                return "September"
            case 10:
                return "Oktober"
            case 11:
                return "November"
            default:
                return "December"
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
 
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -180
        
        print("show")
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
        print("hide")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        workDay=["date":dateText, "time":timeLabel.text! , "note":notesLabel.text!, "movements":doneMovementsLabel.text!, "clock":clockLabel.text!]
        
        movements.insert(workDay, atIndex: 0)
        
        let workData = NSKeyedArchiver.archivedDataWithRootObject(movements)
        NSUserDefaults.standardUserDefaults().setObject(workData, forKey: "workdays")
    }
    
    func timeString() ->String
    {
        
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        let timeString = formatter.stringFromDate(date)
        
        
       return timeString
    }
    @IBAction func takePhoto(sender: AnyObject) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
        imageView.image = image
        
        let myImageName = dateText+clockLabel.text!
        let imagePath = fileInDocumentsDirectory(myImageName)
        
        if let imageToSave = imageView.image {
            saveImage(imageToSave, path: imagePath)
            
        } else { print("error saving image") }
        
    }
    
    
    func saveImage (image: UIImage, path: String ) -> Bool{
        let pngImageData = UIImagePNGRepresentation(image)
        
        let result = pngImageData!.writeToFile(path, atomically: true)
        
        return result
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }
  
}

