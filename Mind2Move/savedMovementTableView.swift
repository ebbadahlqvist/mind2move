//
//  savedMovementTableView.swift
//  Mind2Move
//
//  Created by Ebba on 2016-04-15.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import MessageUI

class savedMovementTableView: UIViewController, UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate {
    
    var movements: [Dictionary<String,String>] = []

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let customFont = UIFont(name: "Aloha", size: 17.0)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)
        navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        let workData = NSUserDefaults.standardUserDefaults().objectForKey("workdays") as? NSData
        
        if let workData = workData {
            movements = NSKeyedUnarchiver.unarchiveObjectWithData(workData) as! [Dictionary<String,String>]
            
        }
        
        navigationItem.title = "Sparade"
        navigationItem.setHidesBackButton(true, animated:true);
        
        tableView.scrollRectToVisible(CGRectMake(0,0,0,0), animated: false)
       
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movements.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        if (cell == nil) {
            cell = UITableViewCell(style: .Value1, reuseIdentifier: "cell")
        }
        
        var dic : Dictionary<String,String> = movements[indexPath.row]
        cell!.textLabel?.text = dic["date"]
        cell!.detailTextLabel?.text = dic["time"]
        
        return cell!
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
         print("You selected cell #\(indexPath.row)!")
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            movements.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            let workData = NSKeyedArchiver.archivedDataWithRootObject(movements)
            NSUserDefaults.standardUserDefaults().setObject(workData, forKey: "workdays")
        }
    }
  
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
       let indexPath = self.tableView.indexPathForSelectedRow
        
        if (segue.identifier == "detailSegue"){
            
            let destinationVC = segue.destinationViewController as! savedWorkdayDetailView
            var dic : Dictionary<String,String> = movements[indexPath!.row]
            
            destinationVC.dateText = dic["date"]!
            destinationVC.timeText = dic["time"]!
            destinationVC.notesText = dic["note"]!
            destinationVC.movementsDoneText = dic["movements"]!
            destinationVC.clockText = dic["clock"]!

        }
    
    }

    @IBAction func makeEmailButton(sender: AnyObject) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setSubject("Mina arbetstider")
        mailComposerVC.setMessageBody(createEmail(), isHTML: false)
        
        return mailComposerVC
    }
    
    func createEmail() ->String{
        
        var emailText = "Mina arbetstider: \n \n"
        
        for item in movements {
            let dic : Dictionary<String,String> = item
            emailText += dic["date"]! + " " + dic["clock"]! + " " + dic["time"]! + "\n \n"}
        
        return emailText + "Mailet är skapat utifrån mina sparade arbetstider i Mind2Move."
    }
    
    func showSendMailErrorAlert() {
        let alertView = UIAlertController(title: "Kunde inte skicka", message: "Din telefon kunde inte skicka mail.", preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertView, animated: true, completion: nil)
        alertView.view.tintColor = UIColor.blackColor()

    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
