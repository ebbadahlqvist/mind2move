//
//  WorkView.swift
//  Mind2Move
//
//  Created by Ebba on 2016-03-29.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit
import AudioToolbox


class WorkView: UIViewController {
    
    var pressedPause = false
    var readyToLeave = true
    var pauseButtonClicked = false
    
    var totalSeconds = 0;
    var currentMove = 0;
    var currentSeconds = 0;
    var oldSeconds = 0
    var movementsDone = 0
    var oldCurrentSeconds = 0
    
    var pauseDate = NSDate()
    var firstDate = NSDate()
    var startDate = NSDate()
    
    var data = Data()
    var moveList = [Movement]()
    var SwiftTimer = NSTimer()
   
    @IBOutlet var infoTextView: UITextView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var pauseButton: UIButton!
   
  
    @IBAction func keepWorkingButton(sender: AnyObject) {
        keepWorking()
    }
    
    override func viewDidLoad(){
        
        firstDate = NSDate()
        startTimer()
        startDate = NSDate()
        
        navigationItem.setHidesBackButton(true, animated:true)
        moveList = data.moveList1
        
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: Double(moveList[0].intervall))
        notification.alertBody = "Nästa rörelse väntar på dig!"
        notification.soundName = UILocalNotificationDefaultSoundName
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
      
        
    }
    
    
    
    func updateCounter() {
        
        let newDate = NSDate()
        
        
       totalSeconds = (NSCalendar.currentCalendar().components(.Second, fromDate: firstDate, toDate: newDate, options: []).second) + oldSeconds
        
        timeLabel.text = timeStringAsText(totalSeconds)
        
     
        currentSeconds = NSCalendar.currentCalendar().components(.Second, fromDate: firstDate, toDate: newDate, options: []).second + oldCurrentSeconds
        
        if(currentSeconds > moveList[currentMove].intervall){
            playSoundClip()
            pauseButton.hidden = true
            popUpView.hidden = false
            SwiftTimer.invalidate()
            infoTextView.text = moveList[currentMove].info;
            infoImage.image = UIImage(named: moveList[currentMove].imageName)
            currentMove+=1
            pauseDate = NSDate()
            oldSeconds = totalSeconds
            movementsDone += 1
            
            if(currentMove == moveList.count){
                currentMove = 0
            }
            currentSeconds = 0
            oldCurrentSeconds = 0
            
        }
    }
    
    func startTimer(){
        
        popUpView.hidden = true
        pauseButton.hidden = false
        SwiftTimer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: #selector(WorkView.updateCounter), userInfo: nil, repeats: true)
    }
   
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeStringAsText (seconds : Int) -> String {
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds)
        return "\(h) h \(m) min \(s) sek"
    }
  
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let destinationVC = segue.destinationViewController as! EditMoveController
        destinationVC.totalTime = totalSeconds
        destinationVC.doneMovementsNumber = movementsDone
        destinationVC.startTime = startDateString()
        
        SwiftTimer.invalidate()
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
    }
    
    @IBAction func playSoundClip() {
        if let soundURL = NSBundle.mainBundle().URLForResource("reminderBeep", withExtension: "wav") {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundURL, &mySound)
            // Play
            AudioServicesPlaySystemSound(mySound);
        }
        
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String,sender: AnyObject?) -> Bool {
        
        if(popUpView.hidden){
        
            let alertView = UIAlertController(title: "Är du säker?", message: "Har du verkligen jobbat färdigt för dagen", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Ja", style: .Default, handler: {action in self.performSegueWithIdentifier("doneSegue", sender: nil)}))
            alertView.addAction(UIAlertAction(title: "Nej", style: .Default, handler: {action in self.keepGoingAfterBreak()}))
            presentViewController(alertView, animated: true, completion: nil)
            alertView.view.tintColor = UIColor.blackColor()
            
        } else {
        
            let alertView = UIAlertController(title: "Är du säker?", message: "Har du verkligen jobbat färdigt för dagen", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Ja", style: .Default, handler: {action in self.performSegueWithIdentifier("doneSegue", sender: nil)}))
            alertView.addAction(UIAlertAction(title: "Nej", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            alertView.view.tintColor = UIColor.blackColor()
        }
        
        
        return true
    }
    
    func keepGoingAfterBreak(){
        oldCurrentSeconds = currentSeconds
        oldSeconds = totalSeconds
        oldSeconds = totalSeconds
        
        createNotification(NSDate(timeIntervalSinceNow: Double(moveList[currentMove].intervall-oldCurrentSeconds)))
        
        keepWorking()
    }
   
    func keepWorking(){
    
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        firstDate = NSDate()
        
        print(movementsDone)
        
        createNotification(NSDate(timeIntervalSinceNow: Double(moveList[currentMove].intervall)))
        
        startTimer()
    }
    
    @IBAction func pauseButton(sender: AnyObject) {
        
        
        if(pauseButtonClicked){
            pauseButtonClicked = false
            keepGoingAfterBreak()
            pauseButton.setTitle("Pause", forState: .Normal)
            
        } else {
            pauseButtonClicked = true
            SwiftTimer.invalidate()
            pauseButton.setTitle("Start", forState: .Normal)
            UIApplication.sharedApplication().cancelAllLocalNotifications()
        }
        
    }
    
    func startDateString() ->String
{
        
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        let timeString = formatter.stringFromDate(startDate)
    
        return timeString
    }
    
    func createNotification(timeToNext: NSDate){
        let notification = UILocalNotification()
        notification.fireDate = timeToNext
        notification.alertBody = "Nästa rörelse väntar på dig!"
        notification.alertAction = "se nästa rörelse."
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["CustomField1": "w00t"]
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
}
