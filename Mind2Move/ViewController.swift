//
//  ViewController.swift
//  Mind2Move
//
//  Created by Ebba on 2016-03-29.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        let customFont = UIFont(name: "Aloha", size: 17.0)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: customFont!], forState: UIControlState.Normal)
        navigationController?.navigationBar.tintColor = UIColor.blackColor()
  
    }
}

